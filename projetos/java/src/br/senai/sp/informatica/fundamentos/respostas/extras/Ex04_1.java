package br.senai.sp.informatica.fundamentos.respostas.extras;

import static br.senai.sp.informatica.lib.InputUtil.*;

public class Ex04_1 {
	public static void main(String[] args) {
		int num1 = leInteiro("Informe o 1º nº");
		int num2 = leInteiro("Informe o 2º nº");
		 
		escreva(num1 == num2 ? "São iguais" : "São diferentes");
	}
}
