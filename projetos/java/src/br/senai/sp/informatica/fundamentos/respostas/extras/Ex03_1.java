package br.senai.sp.informatica.fundamentos.respostas.extras;

import static br.senai.sp.informatica.lib.InputUtil.escreva;
import static br.senai.sp.informatica.lib.InputUtil.leReal;

public class Ex03_1 {
	public static void main(String[] args) {
		double base = leReal("Informe a base");
		 
		double altura = leReal("Informe a altura");

		escreva("A Área é de: ", base * altura / 2);
	}
}
