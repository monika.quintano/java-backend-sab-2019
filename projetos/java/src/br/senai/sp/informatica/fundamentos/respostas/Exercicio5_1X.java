package br.senai.sp.informatica.fundamentos.respostas;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

import static br.senai.sp.informatica.lib.InputUtil.*;

public class Exercicio5_1X {
	public static void main(String[] args) {
		
		AtomicInteger i = new AtomicInteger(1);
		
		escreva("Nºs Ordenados\n\n",
			Stream.generate(() -> leInteiro("Informe o ", i.getAndIncrement(), "º nº"))
				.limit(5)
				.sorted(Comparator.reverseOrder())
				.map(num -> String.valueOf(num))
				.collect(joining(" "))
			);
	}
}

