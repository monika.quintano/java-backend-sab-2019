package br.senai.sp.informatica.fundamentos.objetos;

import java.util.*;

import lombok.Data;

@Data
public class Fatura {
	private String cliente;
	private String enderecoCobranca;
	private String cnpj;
	private List<ItemDaFatura> itens;
	
	public double getTotal() {
		double total = 0;
		
		for (int i = 0; i < itens.size(); i++) {
			total += itens.get(i).getTotal();
		}
		
		for (ItemDaFatura item : itens) {
			total += item.getTotal();
		}
		
		return total;
	}
}

@Data
class ItemDaFatura {
	private String produto;
	private String descricao;
	private double valorUnitario;
	private int quantidade;
	
	public double getTotal() {
		return valorUnitario * quantidade;
	}
}
