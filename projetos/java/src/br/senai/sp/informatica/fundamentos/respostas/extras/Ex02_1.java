package br.senai.sp.informatica.fundamentos.respostas.extras;

import javax.swing.JOptionPane;

public class Ex02_1 {
	public static void main(String[] args) {
		double media = 0;
		
		String temp = JOptionPane.showInputDialog("Informe o 1º nota");
		media += Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 2º nota");
		media += Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 3º nota");
		media += Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 4º nota");
		media += Integer.parseInt(temp);
				
		JOptionPane.showMessageDialog(null, String.format("A média é: %,.2f", media /= 4));
	}
}
