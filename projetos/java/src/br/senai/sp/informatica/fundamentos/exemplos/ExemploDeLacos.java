package br.senai.sp.informatica.fundamentos.exemplos;

import java.util.Arrays;
import static java.util.stream.Collectors.*;

public class ExemploDeLacos {
	public static void main(String[] args) {
		//              0  1  2  3  4  5  6  7  8  9
		int[] lista = { 2, 5, 6, 7, 3, 5, 1, 7, 9, 3 };
		
//		int i = 0;
//		while (i < lista.length) {
//			System.out.print(lista[i] * 2  + " ");
//			i++;
//		}
		
//		for (int i = 0;i < lista.length;i++) {
//			System.out.print(lista[i] * 2  + " ");
//		}
		
//		for (int num : lista) {
//			System.out.print(num * 2  + " ");
//		}
	
		System.out.println(Arrays.stream(lista)
			.mapToObj(num -> String.valueOf(num * 2))
			.collect(joining(" "))
		);
	}
}
