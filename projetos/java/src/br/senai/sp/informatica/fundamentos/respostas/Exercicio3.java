package br.senai.sp.informatica.fundamentos.respostas;

public class Exercicio3 {
	public static void main(String[] args) {
		int total = 1;
		for (int i = 1; i < 16; i++) {
			if(i % 2 != 0) {
				System.out.print(i + " x ");
				total *= i;
			}
		}
		System.out.println("\nTotal: " + total);
	}
}
