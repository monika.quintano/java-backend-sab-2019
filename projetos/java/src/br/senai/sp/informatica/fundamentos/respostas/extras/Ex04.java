package br.senai.sp.informatica.fundamentos.respostas.extras;

import static br.senai.sp.informatica.lib.InputUtil.*;

public class Ex04 {
	public static void main(String[] args) {
		int num1 = leInteiro("Informe o 1º nº");
		int num2 = leInteiro("Informe o 2º nº");
		 
		if(num1 == num2) 
			escreva("São iguais");
		else 
			escreva("São diferentes");
	}
}
