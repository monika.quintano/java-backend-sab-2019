package br.senai.sp.informatica.fundamentos.respostas.extras;

import static br.senai.sp.informatica.lib.InputUtil.*;

public class Ex06 {
	public static void main(String[] args) {
		int A = leInteiro("Informe o valor de A");
		int B = leInteiro("Informe o valor de B");
		 
		escreva(A % B == 0 ? "É par" : "É impar");
	}
}
