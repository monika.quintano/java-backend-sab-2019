package br.senai.sp.informatica.fundamentos.exemplos;

import javax.swing.JOptionPane;

public class CalculoDeJuros3 {
	public static void main(String[] args) {
		String aux = JOptionPane.showInputDialog("Informe o Preço");
		double preco = Double.parseDouble(aux);
				
		while(preco > 0) {
			double taxa;
			
			if(preco > 5000) {
				taxa = .1;
			} else {
				taxa = .05;
			}
			
			double valorFinal = preco * taxa;
			
			JOptionPane.showMessageDialog(null, 
					"O valor final é de " + valorFinal);
			
			aux = JOptionPane.showInputDialog("Informe o Preço");
			preco = Double.parseDouble(aux);
		}
	}
}
