package br.senai.sp.informatica.fundamentos.respostas.extras;

import javax.swing.JOptionPane;

public class Ex01 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe o 1º nº");
		int num1 = Integer.parseInt(temp);
				
		temp = JOptionPane.showInputDialog("Informe o 2º nº");
		int num2 = Integer.parseInt(temp);
		
		double resultado = Math.pow(num1, num2);
		
		JOptionPane.showMessageDialog(null, "O Total: " + resultado);
	}
}
