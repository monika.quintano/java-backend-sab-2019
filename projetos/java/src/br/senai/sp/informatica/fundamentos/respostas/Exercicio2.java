package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

public class Exercicio2 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe a 1ª nota");
		int nota1 = Integer.parseInt(temp);
		
		temp = JOptionPane.showInputDialog("Informe a 2ª nota");
		int nota2 = Integer.parseInt(temp);
		
		double media = (nota1 + nota2) / 2;
		
		String situacao = String.format("A sua média é: %.0f - ", media);
		if(media <= 60) {
			situacao += "Insuficiente";
		} else if(media <= 80) {
			situacao += "Satisfatória";
		} else if(media <= 90) {
			situacao += "Boa";
		} else {
			situacao += "Excelente";
		}
		
		JOptionPane.showMessageDialog(null, situacao);
	}
}
