package br.senai.sp.informatica.fundamentos.respostas.extras;

import javax.swing.JOptionPane;

public class Ex03 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe a base");
		double base = Double.parseDouble(temp);

		temp = JOptionPane.showInputDialog("Informe a altura");
		double altura = Double.parseDouble(temp);

		JOptionPane.showMessageDialog(null, 
				String.format("A Área é de: %,.2f", base * altura / 2));
	}
}
