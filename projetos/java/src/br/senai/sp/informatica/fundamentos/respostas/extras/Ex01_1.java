package br.senai.sp.informatica.fundamentos.respostas.extras;

import java.util.Locale;

import javax.swing.JOptionPane;

public class Ex01_1 {
	public static void main(String[] args) {
		try {
			String temp = JOptionPane.showInputDialog("Informe o 1º nº");
			int num1 = Integer.parseInt(temp);
					
			temp = JOptionPane.showInputDialog("Informe o 2º nº");
			int num2 = Integer.parseInt(temp);
			
			double resultado = Math.pow(num1, num2);
			
			JOptionPane.showMessageDialog(null, 
			  String.format(new Locale("en", "US"),"O Total: %.2f", resultado));
		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(null, "O Número informado é inválido");
		}
	}
}
