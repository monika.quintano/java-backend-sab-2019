package br.senai.sp.informatica.fundamentos.respostas.extras;

import javax.swing.JOptionPane;

public class Ex02_3 {
	public static void main(String[] args) {
		double media = 0;

		int i = 1;
		while (i <= 4) {
			String temp = JOptionPane.showInputDialog("Informe o "+ i++ +"º nota");
			media += Integer.parseInt(temp);
		}
				
		JOptionPane.showMessageDialog(null, String.format("A média é: %,.2f", media /= 4));
	}
}
