package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

import static br.senai.sp.informatica.fundamentos.respostas.Utilitarios.*;

public class Exercicio1_3 {
	public static void main(String[] args) {
		
		double salHora = leValor("Informe o Salário Hora");
		String msg = String.format("Sal. Hora R$ %,.2f\n", salHora);
			
		int horas = leNumero("Informe a Qtd de Horas trabalhadas");
		msg += "Nº de Horas: " + horas + "\n" ;
		
		int dep = leNumero("Informe nº de Dependentes");
		msg += "Nº de Dependentes: " + dep + "\n";
		
		double salBruto = horas * salHora + 50 * dep;
		msg += String.format("Sal. Bruto R$ %,.2f\n", salBruto);
		
		double inss;
		if(salBruto <= 1000) {
			inss = salBruto * (8.5 / 100);
		} else {
			inss = salBruto * (9d / 100);
		}
		msg += String.format("Valor do INSS R$ %,.2f\n", inss);
		
		double ir;
		if(salBruto <= 500) {
			ir = 0;
		} else if(salBruto <= 1000) {
			ir = salBruto * (5d / 100);
		} else {
			ir = salBruto * (7d / 100);
		}
		msg += String.format("Valor do IR R$ %,.2f\n", ir);
		
		double salLiq = salBruto - inss - ir;
		msg += String.format("Sal. Liq. R$ %,.2f", salLiq);
		
		JOptionPane.showMessageDialog(null, msg);
	}

}
