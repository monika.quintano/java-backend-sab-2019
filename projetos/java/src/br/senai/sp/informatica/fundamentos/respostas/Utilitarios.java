package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

public class Utilitarios {
	public static double leValor(String mensagem) {
		String temp = JOptionPane.showInputDialog(mensagem);
		return Double.parseDouble(temp);
	}
	
	public static int leNumero(String mensagem) {
		String temp = JOptionPane.showInputDialog(mensagem);
		return Integer.parseInt(temp);
	}
}
